import React from "react"

import Layout from "../components/layout"
import Link from "../components/link"
import Hero, { heroOptions } from "../components/hero"
import Container from "../components/container"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not Found" />
    <Hero title="404: Page Not Found" color={heroOptions.pinkBlue} />
    <Container>
      <p>
        The page you are looking for is not found. You can{" "}
        <Link to="/">go to the homepage</Link> to see all event posts.
      </p>
    </Container>
  </Layout>
)

export default NotFoundPage
