---
title: Introduction to Python
date: 2020-09-17
time: 7:00 PM
location: Discord
author: Charles Daniels
summary: "How to get started with Python."
---

Python is a hugely popular, easy-to-use language with a rich library ecosystem. It has taken both academic and industry computing by storm, and for good reason. This talk provides a crash course in introductory Python. Basic programming knowledge is assumed, but previous Python experience is not.

## Links

The slides for this talk are [available here](http://cdaniels.net/talks.html#introduction_to_python).
