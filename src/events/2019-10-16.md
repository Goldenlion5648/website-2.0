---
title: Code-a-thon Prep
date: 2019-10-16
time: 7:00 PM
location: Swearingen 2A17
author: Hunter Damron and Justim Baum
tags:
  - "#ThereWillBePizzaToo!"
  - "🍕🍕🍕🍕🍕"
summary: Tips and Tricks for the upcoming ACM Code-a-thon
---

Join us tonight for free pizza followed by a talk lead by Hunter Damron and Justin Baum.

Hunter and Justin will be covering algorithms and problems that are frequently tested in code-a-thons (!), interviews, and upper level undergrad courses. They plan to discuss a variety of topics from the basics to more experienced algorithms that "can help you take your programming skills to the next level". As always, there will be something for everyone - though this talk is especially useful for those of you that are planning to participate in our ACM 24-hour Code-a-thon this Friday and Saturday from 7pm to 7pm.

More information about the Code-a-thon is available [on our site](/events/2019-10-18).

We hope to see you there!
