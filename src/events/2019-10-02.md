---
title: Intro to Machine Learning w/ Python and TensorFlow
date: 2019-10-02
time: 7:00 PM
location: Swearingen 2A17
author: Hunter Damron
tags:
  - "Talk"
summary: Introduction to Machine Learning
---

Join us _tonight_ at 7pm in the CSE Student Lounge (SWGN 2A17) for free pizza followed by a talk by our club president and "good guy" Hunter Damron on machine learning.

Hunter describes the talk:

> This talk is intended to give a beginner-friendly introduction to machine learning using Python and the Keras/TensorFlow library. Neural networks have been successful in many different types of problems recently, from image recognition to board game strategy. Although the theory behind machine learning is complex, the Keras library makes it very easy to set up a simple neural network. We will be working in Python, but no experience is necessary to follow along. We look forward to seeing you, and we invite you to bring a friend from outside the department!

We hope to see you there!

---

### Update

Here is a link to [the slides](https://github.com/hdamron17/ACM_Machine_Learning_Talk/releases).
